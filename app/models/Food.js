export default class Food{
    constructor(ma,tenMon,loaiMon,giaMon,khuyenMai,tinhTrang,hinhMon,moTa){
        this.ma =ma;
        this.tenMon = tenMon;
        this.loai = loaiMon;
        this.giaMon = giaMon;
        this.khuyenMai = khuyenMai;
        this.tinhTrang = tinhTrang;
        this.hinhMon = hinhMon;
        this.moTa = moTa
    }
    tinhGiaKM = function (){
        return this.giaMon*(1-this.khuyenMai/100)
    }
}