import Food from "../models/Food.js";

const BASE_LOAI = true;
const BASE_STATUS = true;
export let listFood = (list) => {
    let tableListFood = "";
    list.forEach(item => {
        let contentString = `<tr>
            <td>${item.ma}</td>
            <td>${item.tenMon}</td>
            <td>${item.loai == "loai2" ? "Mặn" : "Chay"}</td>
            <td> ${VND.format(item.giaMon)}</td>
            <td>${item.khuyenMai}</td>
            <td>${VND.format(item.giaKhuyenMai)}</td>
            <td>${item.tinhTrang == BASE_STATUS ? "Còn hàng" : "Hết hàng"}</td>
            <td>
            <button class="btn btn-danger" onclick ="delFood(${item.ma})">Xoá</button>
            <button data-toggle="modal" data-target="#exampleModal" class="btn btn-primary" onclick ="editFood(${item.ma})">Chỉnh sửa</button>
            </td>
         
        </tr>`;
        tableListFood += contentString
    });
    document.querySelector("#tbodyFood").innerHTML = tableListFood
}

export let getInfoFood = () => {
    let monAn = new Food()
    monAn.ma = document.querySelector("#foodID").value
    monAn.tenMon = document.querySelector("#tenMon").value
    monAn.loai = document.querySelector("#loai").value
    monAn.giaMon = document.querySelector("#giaMon").value
    monAn.khuyenMai = document.querySelector("#khuyenMai").value
    monAn.tinhTrang = document.querySelector("#tinhTrang").value
    monAn.hinhMon = document.querySelector("#hinhMon").value
    monAn.giaKhuyenMai = monAn.tinhGiaKM()
    monAn.moTa = document.querySelector("#moTa").value
    var valid =
        validation.kiemTraRong(monAn.tenMon, "invalidTen", "Tên món ăn") & validation.kiemTraTatCaKyTu(monAn.tenMon, "invalidTenKyTu", "Tên món ăn") & validation.kiemTraRong(monAn.loai, "invalidLoai", "Loại món ăn") & validation.kiemTraRong(monAn.giaMon, "invalidGia", "Giá món ăn") & validation.kiemtraTatCaLaSo(monAn.giaMon, "invalidGiaNumber", "Giá món ăn") & validation.kiemTraRong(monAn.khuyenMai, "invalidKM", "Khuyến mãi món ăn") & validation.kiemTraRong(monAn.tinhTrang, "invalidTT", "Tình trạng món ăn") & validation.kiemTraRong(monAn.hinhMon, "invalidHinhMon", "Hình ảnh món ăn") & validation.kiemTraRong(monAn.moTa, "invalidMoTa", "Mô tả món ăn");
    if (!valid) {
        return;
    }

    return monAn;
}


export let getEdit = (data) => {
    console.log(data);
    document.querySelector("#foodID").value = data.ma
    document.querySelector("#tenMon").value = data.tenMon
    document.querySelector("#loai").value = data.loai==true?"loai1":"loai2"
    document.querySelector("#giaMon").value = data.giaMon
    document.querySelector("#khuyenMai").value = data.khuyenMai
    document.querySelector("#tinhTrang").value = data.tinhTrang
    document.querySelector("#hinhMon").value = data.hinhMon
    document.querySelector("#moTa").value = data.moTa
}

export let clearForm = ()=>{
    document.querySelector("#foodID").value = ""
    document.querySelector("#tenMon").value = ""
    document.querySelector("#loai").value =""
    document.querySelector("#giaMon").value = ""
    document.querySelector("#khuyenMai").value =""
    document.querySelector("#tinhTrang").value = ""
    document.querySelector("#hinhMon").value = ""
    document.querySelector("#moTa").value = ""
}

