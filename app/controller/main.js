import { listFood, getInfoFood, getEdit,clearForm } from "./controller.js"
import foodServ from '../service/service.js'
import Food from "../models/Food.js"
let fletchListFood = () => {
    foodServ.getListServ().then((res) => {
        listFood(res.data)
    })
}

fletchListFood();

let addFood = () => {
    let data = getInfoFood();
    if (data != null) {
        foodServ.addFoodServ(data).then(fletchListFood);
    }
    clearForm()
}

window.addFood = addFood

let delFood = (id) => {
    Swal.fire({
        title: 'Bạn có chắc muốn xoá dòng này?',
        text: "Xoá đi sẽ không thu hồi lại được!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.isConfirmed) {
            foodServ.delFoodServ(id).then(fletchListFood)
            Swal.fire(
                'Deleted!',
                'Dữ liệu đã được xoá',
                'success'
            )
        }
    })
    // 
}

window.delFood = delFood

let editFood = (id)=>{
    foodServ.getFoodById(id).then((res)=>{
        getEdit(res.data)
    })
}

window.editFood = editFood

let updateFood = () => {
    let arrFood
    foodServ.getListServ().then((res) => {
        arrFood = res.data
        let data = getInfoFood();
        let index = arrFood.findIndex(food=>food.ma===data.ma);
        if(index!=-1){
            let foodUpdate =  arrFood[index]
            foodUpdate.tenMon = data.tenMon
            foodUpdate.loai = data.loai
            foodUpdate.giaMon = data.giaMon
            foodUpdate.khuyenMai = data.khuyenMai
            foodUpdate.tinhTrang = data.tinhTrang
            foodUpdate.hinhMon = data.hinhMon
            foodUpdate.moTa = data.moTa
            foodUpdate.giaKhuyenMai = Number(foodUpdate.giaMon)*(1-Number(foodUpdate.khuyenMai)/100)
            foodServ.updateFoodById(foodUpdate).then(fletchListFood)
        }     
    })

   
}

window.updateFood = updateFood

let findByType = () => {
    let value = document.getElementById("selLoai").value;
    let arrFoodByType =[]
    foodServ.getListServ().then((res) => {
       res.data.forEach(item => {
            if(item.loai ===value){
                arrFoodByType.push(item)
            }else if(value==="all"){
                arrFoodByType.push(item)
                return;
            }
       });
       listFood(arrFoodByType)
    })
}

window.findByType = findByType
