const BASE_URL = "https://64d84e895f9bf5b879ce35bf.mockapi.io/food"
let getListServ = () => {
    return axios({
        url: BASE_URL,
        method: "GET"
    })
}

let delFoodServ = (id) => {
    return axios({
        url: `${BASE_URL}/${id}`,
        method: "DELETE"
    })
}

let addFoodServ = (monAn) => {
    
    return axios({
        url: `${BASE_URL}`,
        method: "POST",
        data:monAn
    })
}

let getFoodById = (id)=>{
    return axios({
        url: `${BASE_URL}/${id}`,
        method: "GET"
    })
}

let updateFoodById = (data)=>{
    return axios({
        url: `${BASE_URL}/${data.ma}`,
        method: "PUT",
        data:data
    })
}

let foodServ = {
    getListServ,
    delFoodServ,
    addFoodServ,
    getFoodById,
    updateFoodById
}

export default foodServ